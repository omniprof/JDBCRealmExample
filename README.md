Currently this application does not work. It always shows jdbcrealm:invaliduser

This application demonstrates how to use server side authentication using the JDBCRealm. It is based on a blog by Markus Eisele that you can find at  http://blog.eisele.net/2013/01/jdbc-realm-glassfish312-primefaces342.html.

Step one is to set up the realm on the server. I am using Payara.
From the Admin Console for Payara:
Select "Configurations > server-config > Security > Realms“
Select "New..." on top of the table. 
Enter a name (e.g. JDBCRealm)
Select the com.sun.enterprise.security.auth.realm.jdbc.JDBCRealm from the drop down. 

Fill in the following values into the text fields in the form that appears:

JAAS Context    jdbcRealm
JNDI    jdbc/securityDatasource
User Table  USERS
User Name Column    USERNAME
Password Column PASSWORD
Group Table GROUPS
Group Table User Name Column    USERNAME
Group Name Column   GROUPNAME

Leave all other fields blank, default values should be sufficient.

The JDBC resource is defined in glassfish-resources.xml

The database script for MySQL can be found in Other Test Sources.



